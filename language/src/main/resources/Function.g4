grammar Function;

program
	:	statement*
	|	EOF
	;

statement
	:	assignment
	|	expression
	;

assignment
	:	VAR ':=' expression
	;

expression
	:	unary																											# unaryExpr
	|	'(' expression ')'																								# groupExpr
	|	expression ( '(' argumentList? ')' )+																			# functionExpr
	|	obj=expression '[' first=expression (':' last=expression)? ']'													# arrayAccessExpr
	|	obj=expression '.' prop=expression																				# memberAccessExpr
	|	list=expression ('as' as=ID)? 'where' cond=expression ('select' select=expression)? ('limit' limit=expression)?	# whereExpr
	|	left=expression op=(MULT|DIV|MOD|EXP) right=expression															# binaryExpr
	|	left=expression op=(ADD|SUB) right=expression																	# binaryExpr
	|	left=expression op=(LT|LE|GT|GE|NE|EQ) right=expression															# binaryExpr
	|	left=expression op=(AND|OR|XOR) right=expression																# logicExpr
	|	arrayLiteral																									# arrayLiteralExpr
	|	literal																											# literalExpr
	;

arrayLiteral
	:	'[' argumentList? ']'
	;

unary
	:	op=(NOT|BANG|SUB) primary
	;

primary
	:	literal
	|	'(' expression ')'
	;

argumentList
	:	expression (',' expression)*
	;

literal
	:	type=(NUMBER|STRING|BOOLEAN|VAR|ID|NIL)
	;

ADD : '+';
SUB : '-';
MULT : '*';
DIV : '/';
MOD : '%';
EXP : '^';
LT	: '<';
LE	: '<=';
GT	: '>';
GE	: '>=';
EQ	: '='|'==';
NE	: '!=';
AND : 'and';
OR	: 'or';
XOR : 'xor';
NOT : 'not';
BANG : '!';
STRING : '"' (~'"')* '"';
NUMBER : [0-9]+ ('.' [0-9]+)?;
BOOLEAN : 'true'|'false';
NIL : 'nil';
ID  : [a-zA-Z0-9_][a-zA-Z0-9_]*;
VAR : '$' ID;
WS : [ \t\r\n]+ -> skip;