// Generated from Function.g4 by ANTLR 4.7
package org.simo.function.parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({ "all", "warnings", "unchecked", "unused", "cast" })
public class FunctionParser extends Parser {
    static {
        RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION);
    }

    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache = new PredictionContextCache();
    public static final int T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, T__7 = 8, T__8 = 9,
            T__9 = 10, T__10 = 11, T__11 = 12, ADD = 13, SUB = 14, MULT = 15, DIV = 16, MOD = 17, EXP = 18, LT = 19,
            LE = 20, GT = 21, GE = 22, EQ = 23, NE = 24, AND = 25, OR = 26, XOR = 27, NOT = 28, BANG = 29, STRING = 30,
            NUMBER = 31, BOOLEAN = 32, NIL = 33, ID = 34, VAR = 35, WS = 36;
    public static final int RULE_program = 0, RULE_statement = 1, RULE_assignment = 2, RULE_expression = 3,
            RULE_arrayLiteral = 4, RULE_unary = 5, RULE_primary = 6, RULE_argumentList = 7, RULE_literal = 8;
    public static final String[] ruleNames = { "program", "statement", "assignment", "expression", "arrayLiteral",
            "unary", "primary", "argumentList", "literal" };

    private static final String[] _LITERAL_NAMES = { null, "':='", "'('", "')'", "'['", "':'", "']'", "'.'", "'as'",
            "'where'", "'select'", "'limit'", "','", "'+'", "'-'", "'*'", "'/'", "'%'", "'^'", "'<'", "'<='", "'>'",
            "'>='", null, "'!='", "'and'", "'or'", "'xor'", "'not'", "'!'", null, null, null, "'nil'" };
    private static final String[] _SYMBOLIC_NAMES = { null, null, null, null, null, null, null, null, null, null, null,
            null, null, "ADD", "SUB", "MULT", "DIV", "MOD", "EXP", "LT", "LE", "GT", "GE", "EQ", "NE", "AND", "OR",
            "XOR", "NOT", "BANG", "STRING", "NUMBER", "BOOLEAN", "NIL", "ID", "VAR", "WS" };
    public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

    /**
     * @deprecated Use {@link #VOCABULARY} instead.
     */
    @Deprecated
    public static final String[] tokenNames;
    static {
        tokenNames = new String[_SYMBOLIC_NAMES.length];
        for (int i = 0; i < tokenNames.length; i++) {
            tokenNames[i] = VOCABULARY.getLiteralName(i);
            if (tokenNames[i] == null) {
                tokenNames[i] = VOCABULARY.getSymbolicName(i);
            }

            if (tokenNames[i] == null) {
                tokenNames[i] = "<INVALID>";
            }
        }
    }

    @Override
    @Deprecated
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override

    public Vocabulary getVocabulary() {
        return VOCABULARY;
    }

    @Override
    public String getGrammarFileName() {
        return "Function.g4";
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public FunctionParser(TokenStream input) {
        super(input);
        _interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    public static class ProgramContext extends ParserRuleContext {
        public List<StatementContext> statement() {
            return getRuleContexts(StatementContext.class);
        }

        public StatementContext statement(int i) {
            return getRuleContext(StatementContext.class, i);
        }

        public TerminalNode EOF() {
            return getToken(FunctionParser.EOF, 0);
        }

        public ProgramContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_program;
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitProgram(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ProgramContext program() throws RecognitionException {
        ProgramContext _localctx = new ProgramContext(_ctx, getState());
        enterRule(_localctx, 0, RULE_program);
        int _la;
        try {
            setState(25);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 1, _ctx)) {
            case 1:
                enterOuterAlt(_localctx, 1); {
                setState(21);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while ((((_la) & ~0x3f) == 0 && ((1L << _la)
                        & ((1L << T__1) | (1L << T__3) | (1L << SUB) | (1L << NOT) | (1L << BANG) | (1L << STRING)
                                | (1L << NUMBER) | (1L << BOOLEAN) | (1L << NIL) | (1L << ID) | (1L << VAR))) != 0)) {
                    {
                        {
                            setState(18);
                            statement();
                        }
                    }
                    setState(23);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
                break;
            case 2:
                enterOuterAlt(_localctx, 2); {
                setState(24);
                match(EOF);
            }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class StatementContext extends ParserRuleContext {
        public AssignmentContext assignment() {
            return getRuleContext(AssignmentContext.class, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public StatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_statement;
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final StatementContext statement() throws RecognitionException {
        StatementContext _localctx = new StatementContext(_ctx, getState());
        enterRule(_localctx, 2, RULE_statement);
        try {
            setState(29);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 2, _ctx)) {
            case 1:
                enterOuterAlt(_localctx, 1); {
                setState(27);
                assignment();
            }
                break;
            case 2:
                enterOuterAlt(_localctx, 2); {
                setState(28);
                expression(0);
            }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AssignmentContext extends ParserRuleContext {
        public TerminalNode VAR() {
            return getToken(FunctionParser.VAR, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public AssignmentContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_assignment;
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitAssignment(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final AssignmentContext assignment() throws RecognitionException {
        AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
        enterRule(_localctx, 4, RULE_assignment);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(31);
                match(VAR);
                setState(32);
                match(T__0);
                setState(33);
                expression(0);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ExpressionContext extends ParserRuleContext {
        public ExpressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_expression;
        }

        public ExpressionContext() {
        }

        public void copyFrom(ExpressionContext ctx) {
            super.copyFrom(ctx);
        }
    }

    public static class GroupExprContext extends ExpressionContext {
        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public GroupExprContext(ExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitGroupExpr(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public static class UnaryExprContext extends ExpressionContext {
        public UnaryContext unary() {
            return getRuleContext(UnaryContext.class, 0);
        }

        public UnaryExprContext(ExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitUnaryExpr(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public static class ArrayAccessExprContext extends ExpressionContext {
        public ExpressionContext obj;
        public ExpressionContext first;
        public ExpressionContext last;

        public List<ExpressionContext> expression() {
            return getRuleContexts(ExpressionContext.class);
        }

        public ExpressionContext expression(int i) {
            return getRuleContext(ExpressionContext.class, i);
        }

        public ArrayAccessExprContext(ExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitArrayAccessExpr(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public static class WhereExprContext extends ExpressionContext {
        public ExpressionContext list;
        public Token as;
        public ExpressionContext cond;
        public ExpressionContext select;
        public ExpressionContext limit;

        public List<ExpressionContext> expression() {
            return getRuleContexts(ExpressionContext.class);
        }

        public ExpressionContext expression(int i) {
            return getRuleContext(ExpressionContext.class, i);
        }

        public TerminalNode ID() {
            return getToken(FunctionParser.ID, 0);
        }

        public WhereExprContext(ExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitWhereExpr(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public static class LiteralExprContext extends ExpressionContext {
        public LiteralContext literal() {
            return getRuleContext(LiteralContext.class, 0);
        }

        public LiteralExprContext(ExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitLiteralExpr(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public static class LogicExprContext extends ExpressionContext {
        public ExpressionContext left;
        public Token op;
        public ExpressionContext right;

        public List<ExpressionContext> expression() {
            return getRuleContexts(ExpressionContext.class);
        }

        public ExpressionContext expression(int i) {
            return getRuleContext(ExpressionContext.class, i);
        }

        public TerminalNode AND() {
            return getToken(FunctionParser.AND, 0);
        }

        public TerminalNode OR() {
            return getToken(FunctionParser.OR, 0);
        }

        public TerminalNode XOR() {
            return getToken(FunctionParser.XOR, 0);
        }

        public LogicExprContext(ExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitLogicExpr(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public static class MemberAccessExprContext extends ExpressionContext {
        public ExpressionContext obj;
        public ExpressionContext prop;

        public List<ExpressionContext> expression() {
            return getRuleContexts(ExpressionContext.class);
        }

        public ExpressionContext expression(int i) {
            return getRuleContext(ExpressionContext.class, i);
        }

        public MemberAccessExprContext(ExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitMemberAccessExpr(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public static class BinaryExprContext extends ExpressionContext {
        public ExpressionContext left;
        public Token op;
        public ExpressionContext right;

        public List<ExpressionContext> expression() {
            return getRuleContexts(ExpressionContext.class);
        }

        public ExpressionContext expression(int i) {
            return getRuleContext(ExpressionContext.class, i);
        }

        public TerminalNode MULT() {
            return getToken(FunctionParser.MULT, 0);
        }

        public TerminalNode DIV() {
            return getToken(FunctionParser.DIV, 0);
        }

        public TerminalNode MOD() {
            return getToken(FunctionParser.MOD, 0);
        }

        public TerminalNode EXP() {
            return getToken(FunctionParser.EXP, 0);
        }

        public TerminalNode ADD() {
            return getToken(FunctionParser.ADD, 0);
        }

        public TerminalNode SUB() {
            return getToken(FunctionParser.SUB, 0);
        }

        public TerminalNode LT() {
            return getToken(FunctionParser.LT, 0);
        }

        public TerminalNode LE() {
            return getToken(FunctionParser.LE, 0);
        }

        public TerminalNode GT() {
            return getToken(FunctionParser.GT, 0);
        }

        public TerminalNode GE() {
            return getToken(FunctionParser.GE, 0);
        }

        public TerminalNode NE() {
            return getToken(FunctionParser.NE, 0);
        }

        public TerminalNode EQ() {
            return getToken(FunctionParser.EQ, 0);
        }

        public BinaryExprContext(ExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitBinaryExpr(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public static class ArrayLiteralExprContext extends ExpressionContext {
        public ArrayLiteralContext arrayLiteral() {
            return getRuleContext(ArrayLiteralContext.class, 0);
        }

        public ArrayLiteralExprContext(ExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitArrayLiteralExpr(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public static class FunctionExprContext extends ExpressionContext {
        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public List<ArgumentListContext> argumentList() {
            return getRuleContexts(ArgumentListContext.class);
        }

        public ArgumentListContext argumentList(int i) {
            return getRuleContext(ArgumentListContext.class, i);
        }

        public FunctionExprContext(ExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitFunctionExpr(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ExpressionContext expression() throws RecognitionException {
        return expression(0);
    }

    private ExpressionContext expression(int _p) throws RecognitionException {
        ParserRuleContext _parentctx = _ctx;
        int _parentState = getState();
        ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
        ExpressionContext _prevctx = _localctx;
        int _startState = 6;
        enterRecursionRule(_localctx, 6, RULE_expression, _p);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(43);
                _errHandler.sync(this);
                switch (_input.LA(1)) {
                case SUB:
                case NOT:
                case BANG: {
                    _localctx = new UnaryExprContext(_localctx);
                    _ctx = _localctx;
                    _prevctx = _localctx;

                    setState(36);
                    unary();
                }
                    break;
                case T__1: {
                    _localctx = new GroupExprContext(_localctx);
                    _ctx = _localctx;
                    _prevctx = _localctx;
                    setState(37);
                    match(T__1);
                    setState(38);
                    expression(0);
                    setState(39);
                    match(T__2);
                }
                    break;
                case T__3: {
                    _localctx = new ArrayLiteralExprContext(_localctx);
                    _ctx = _localctx;
                    _prevctx = _localctx;
                    setState(41);
                    arrayLiteral();
                }
                    break;
                case STRING:
                case NUMBER:
                case BOOLEAN:
                case NIL:
                case ID:
                case VAR: {
                    _localctx = new LiteralExprContext(_localctx);
                    _ctx = _localctx;
                    _prevctx = _localctx;
                    setState(42);
                    literal();
                }
                    break;
                default:
                    throw new NoViableAltException(this);
                }
                _ctx.stop = _input.LT(-1);
                setState(96);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 11, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        if (_parseListeners != null)
                            triggerExitRuleEvent();
                        _prevctx = _localctx;
                        {
                            setState(94);
                            _errHandler.sync(this);
                            switch (getInterpreter().adaptivePredict(_input, 10, _ctx)) {
                            case 1: {
                                _localctx = new MemberAccessExprContext(
                                        new ExpressionContext(_parentctx, _parentState));
                                ((MemberAccessExprContext) _localctx).obj = _prevctx;
                                pushNewRecursionContext(_localctx, _startState, RULE_expression);
                                setState(45);
                                if (!(precpred(_ctx, 8)))
                                    throw new FailedPredicateException(this, "precpred(_ctx, 8)");
                                setState(46);
                                match(T__6);
                                setState(47);
                                ((MemberAccessExprContext) _localctx).prop = expression(9);
                            }
                                break;
                            case 2: {
                                _localctx = new BinaryExprContext(new ExpressionContext(_parentctx, _parentState));
                                ((BinaryExprContext) _localctx).left = _prevctx;
                                pushNewRecursionContext(_localctx, _startState, RULE_expression);
                                setState(48);
                                if (!(precpred(_ctx, 6)))
                                    throw new FailedPredicateException(this, "precpred(_ctx, 6)");
                                setState(49);
                                ((BinaryExprContext) _localctx).op = _input.LT(1);
                                _la = _input.LA(1);
                                if (!((((_la) & ~0x3f) == 0 && ((1L << _la)
                                        & ((1L << MULT) | (1L << DIV) | (1L << MOD) | (1L << EXP))) != 0))) {
                                    ((BinaryExprContext) _localctx).op = (Token) _errHandler.recoverInline(this);
                                } else {
                                    if (_input.LA(1) == Token.EOF)
                                        matchedEOF = true;
                                    _errHandler.reportMatch(this);
                                    consume();
                                }
                                setState(50);
                                ((BinaryExprContext) _localctx).right = expression(7);
                            }
                                break;
                            case 3: {
                                _localctx = new BinaryExprContext(new ExpressionContext(_parentctx, _parentState));
                                ((BinaryExprContext) _localctx).left = _prevctx;
                                pushNewRecursionContext(_localctx, _startState, RULE_expression);
                                setState(51);
                                if (!(precpred(_ctx, 5)))
                                    throw new FailedPredicateException(this, "precpred(_ctx, 5)");
                                setState(52);
                                ((BinaryExprContext) _localctx).op = _input.LT(1);
                                _la = _input.LA(1);
                                if (!(_la == ADD || _la == SUB)) {
                                    ((BinaryExprContext) _localctx).op = (Token) _errHandler.recoverInline(this);
                                } else {
                                    if (_input.LA(1) == Token.EOF)
                                        matchedEOF = true;
                                    _errHandler.reportMatch(this);
                                    consume();
                                }
                                setState(53);
                                ((BinaryExprContext) _localctx).right = expression(6);
                            }
                                break;
                            case 4: {
                                _localctx = new BinaryExprContext(new ExpressionContext(_parentctx, _parentState));
                                ((BinaryExprContext) _localctx).left = _prevctx;
                                pushNewRecursionContext(_localctx, _startState, RULE_expression);
                                setState(54);
                                if (!(precpred(_ctx, 4)))
                                    throw new FailedPredicateException(this, "precpred(_ctx, 4)");
                                setState(55);
                                ((BinaryExprContext) _localctx).op = _input.LT(1);
                                _la = _input.LA(1);
                                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LE) | (1L << GT)
                                        | (1L << GE) | (1L << EQ) | (1L << NE))) != 0))) {
                                    ((BinaryExprContext) _localctx).op = (Token) _errHandler.recoverInline(this);
                                } else {
                                    if (_input.LA(1) == Token.EOF)
                                        matchedEOF = true;
                                    _errHandler.reportMatch(this);
                                    consume();
                                }
                                setState(56);
                                ((BinaryExprContext) _localctx).right = expression(5);
                            }
                                break;
                            case 5: {
                                _localctx = new LogicExprContext(new ExpressionContext(_parentctx, _parentState));
                                ((LogicExprContext) _localctx).left = _prevctx;
                                pushNewRecursionContext(_localctx, _startState, RULE_expression);
                                setState(57);
                                if (!(precpred(_ctx, 3)))
                                    throw new FailedPredicateException(this, "precpred(_ctx, 3)");
                                setState(58);
                                ((LogicExprContext) _localctx).op = _input.LT(1);
                                _la = _input.LA(1);
                                if (!((((_la) & ~0x3f) == 0
                                        && ((1L << _la) & ((1L << AND) | (1L << OR) | (1L << XOR))) != 0))) {
                                    ((LogicExprContext) _localctx).op = (Token) _errHandler.recoverInline(this);
                                } else {
                                    if (_input.LA(1) == Token.EOF)
                                        matchedEOF = true;
                                    _errHandler.reportMatch(this);
                                    consume();
                                }
                                setState(59);
                                ((LogicExprContext) _localctx).right = expression(4);
                            }
                                break;
                            case 6: {
                                _localctx = new FunctionExprContext(new ExpressionContext(_parentctx, _parentState));
                                pushNewRecursionContext(_localctx, _startState, RULE_expression);
                                setState(60);
                                if (!(precpred(_ctx, 10)))
                                    throw new FailedPredicateException(this, "precpred(_ctx, 10)");
                                setState(66);
                                _errHandler.sync(this);
                                _alt = 1;
                                do {
                                    switch (_alt) {
                                    case 1: {
                                        {
                                            setState(61);
                                            match(T__1);
                                            setState(63);
                                            _errHandler.sync(this);
                                            _la = _input.LA(1);
                                            if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__3)
                                                    | (1L << SUB) | (1L << NOT) | (1L << BANG) | (1L << STRING)
                                                    | (1L << NUMBER) | (1L << BOOLEAN) | (1L << NIL) | (1L << ID)
                                                    | (1L << VAR))) != 0)) {
                                                {
                                                    setState(62);
                                                    argumentList();
                                                }
                                            }

                                            setState(65);
                                            match(T__2);
                                        }
                                    }
                                        break;
                                    default:
                                        throw new NoViableAltException(this);
                                    }
                                    setState(68);
                                    _errHandler.sync(this);
                                    _alt = getInterpreter().adaptivePredict(_input, 5, _ctx);
                                } while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER);
                            }
                                break;
                            case 7: {
                                _localctx = new ArrayAccessExprContext(new ExpressionContext(_parentctx, _parentState));
                                ((ArrayAccessExprContext) _localctx).obj = _prevctx;
                                pushNewRecursionContext(_localctx, _startState, RULE_expression);
                                setState(70);
                                if (!(precpred(_ctx, 9)))
                                    throw new FailedPredicateException(this, "precpred(_ctx, 9)");
                                setState(71);
                                match(T__3);
                                setState(72);
                                ((ArrayAccessExprContext) _localctx).first = expression(0);
                                setState(75);
                                _errHandler.sync(this);
                                _la = _input.LA(1);
                                if (_la == T__4) {
                                    {
                                        setState(73);
                                        match(T__4);
                                        setState(74);
                                        ((ArrayAccessExprContext) _localctx).last = expression(0);
                                    }
                                }

                                setState(77);
                                match(T__5);
                            }
                                break;
                            case 8: {
                                _localctx = new WhereExprContext(new ExpressionContext(_parentctx, _parentState));
                                ((WhereExprContext) _localctx).list = _prevctx;
                                pushNewRecursionContext(_localctx, _startState, RULE_expression);
                                setState(79);
                                if (!(precpred(_ctx, 7)))
                                    throw new FailedPredicateException(this, "precpred(_ctx, 7)");
                                setState(82);
                                _errHandler.sync(this);
                                _la = _input.LA(1);
                                if (_la == T__7) {
                                    {
                                        setState(80);
                                        match(T__7);
                                        setState(81);
                                        ((WhereExprContext) _localctx).as = match(ID);
                                    }
                                }

                                setState(84);
                                match(T__8);
                                setState(85);
                                ((WhereExprContext) _localctx).cond = expression(0);
                                setState(88);
                                _errHandler.sync(this);
                                switch (getInterpreter().adaptivePredict(_input, 8, _ctx)) {
                                case 1: {
                                    setState(86);
                                    match(T__9);
                                    setState(87);
                                    ((WhereExprContext) _localctx).select = expression(0);
                                }
                                    break;
                                }
                                setState(92);
                                _errHandler.sync(this);
                                switch (getInterpreter().adaptivePredict(_input, 9, _ctx)) {
                                case 1: {
                                    setState(90);
                                    match(T__10);
                                    setState(91);
                                    ((WhereExprContext) _localctx).limit = expression(0);
                                }
                                    break;
                                }
                            }
                                break;
                            }
                        }
                    }
                    setState(98);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 11, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            unrollRecursionContexts(_parentctx);
        }
        return _localctx;
    }

    public static class ArrayLiteralContext extends ParserRuleContext {
        public ArgumentListContext argumentList() {
            return getRuleContext(ArgumentListContext.class, 0);
        }

        public ArrayLiteralContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_arrayLiteral;
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitArrayLiteral(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ArrayLiteralContext arrayLiteral() throws RecognitionException {
        ArrayLiteralContext _localctx = new ArrayLiteralContext(_ctx, getState());
        enterRule(_localctx, 8, RULE_arrayLiteral);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(99);
                match(T__3);
                setState(101);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la)
                        & ((1L << T__1) | (1L << T__3) | (1L << SUB) | (1L << NOT) | (1L << BANG) | (1L << STRING)
                                | (1L << NUMBER) | (1L << BOOLEAN) | (1L << NIL) | (1L << ID) | (1L << VAR))) != 0)) {
                    {
                        setState(100);
                        argumentList();
                    }
                }

                setState(103);
                match(T__5);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class UnaryContext extends ParserRuleContext {
        public Token op;

        public PrimaryContext primary() {
            return getRuleContext(PrimaryContext.class, 0);
        }

        public TerminalNode NOT() {
            return getToken(FunctionParser.NOT, 0);
        }

        public TerminalNode BANG() {
            return getToken(FunctionParser.BANG, 0);
        }

        public TerminalNode SUB() {
            return getToken(FunctionParser.SUB, 0);
        }

        public UnaryContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_unary;
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitUnary(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final UnaryContext unary() throws RecognitionException {
        UnaryContext _localctx = new UnaryContext(_ctx, getState());
        enterRule(_localctx, 10, RULE_unary);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(105);
                ((UnaryContext) _localctx).op = _input.LT(1);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SUB) | (1L << NOT) | (1L << BANG))) != 0))) {
                    ((UnaryContext) _localctx).op = (Token) _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF)
                        matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
                setState(106);
                primary();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class PrimaryContext extends ParserRuleContext {
        public LiteralContext literal() {
            return getRuleContext(LiteralContext.class, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public PrimaryContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_primary;
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitPrimary(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final PrimaryContext primary() throws RecognitionException {
        PrimaryContext _localctx = new PrimaryContext(_ctx, getState());
        enterRule(_localctx, 12, RULE_primary);
        try {
            setState(113);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
            case STRING:
            case NUMBER:
            case BOOLEAN:
            case NIL:
            case ID:
            case VAR:
                enterOuterAlt(_localctx, 1); {
                setState(108);
                literal();
            }
                break;
            case T__1:
                enterOuterAlt(_localctx, 2); {
                setState(109);
                match(T__1);
                setState(110);
                expression(0);
                setState(111);
                match(T__2);
            }
                break;
            default:
                throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ArgumentListContext extends ParserRuleContext {
        public List<ExpressionContext> expression() {
            return getRuleContexts(ExpressionContext.class);
        }

        public ExpressionContext expression(int i) {
            return getRuleContext(ExpressionContext.class, i);
        }

        public ArgumentListContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_argumentList;
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitArgumentList(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ArgumentListContext argumentList() throws RecognitionException {
        ArgumentListContext _localctx = new ArgumentListContext(_ctx, getState());
        enterRule(_localctx, 14, RULE_argumentList);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(115);
                expression(0);
                setState(120);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__11) {
                    {
                        {
                            setState(116);
                            match(T__11);
                            setState(117);
                            expression(0);
                        }
                    }
                    setState(122);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class LiteralContext extends ParserRuleContext {
        public Token type;

        public TerminalNode NUMBER() {
            return getToken(FunctionParser.NUMBER, 0);
        }

        public TerminalNode STRING() {
            return getToken(FunctionParser.STRING, 0);
        }

        public TerminalNode BOOLEAN() {
            return getToken(FunctionParser.BOOLEAN, 0);
        }

        public TerminalNode VAR() {
            return getToken(FunctionParser.VAR, 0);
        }

        public TerminalNode ID() {
            return getToken(FunctionParser.ID, 0);
        }

        public TerminalNode NIL() {
            return getToken(FunctionParser.NIL, 0);
        }

        public LiteralContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_literal;
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof FunctionVisitor)
                return ((FunctionVisitor<? extends T>) visitor).visitLiteral(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final LiteralContext literal() throws RecognitionException {
        LiteralContext _localctx = new LiteralContext(_ctx, getState());
        enterRule(_localctx, 16, RULE_literal);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(123);
                ((LiteralContext) _localctx).type = _input.LT(1);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STRING) | (1L << NUMBER) | (1L << BOOLEAN)
                        | (1L << NIL) | (1L << ID) | (1L << VAR))) != 0))) {
                    ((LiteralContext) _localctx).type = (Token) _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF)
                        matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
        switch (ruleIndex) {
        case 3:
            return expression_sempred((ExpressionContext) _localctx, predIndex);
        }
        return true;
    }

    private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
        switch (predIndex) {
        case 0:
            return precpred(_ctx, 8);
        case 1:
            return precpred(_ctx, 6);
        case 2:
            return precpred(_ctx, 5);
        case 3:
            return precpred(_ctx, 4);
        case 4:
            return precpred(_ctx, 3);
        case 5:
            return precpred(_ctx, 10);
        case 6:
            return precpred(_ctx, 9);
        case 7:
            return precpred(_ctx, 7);
        }
        return true;
    }

    public static final String _serializedATN = "\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3&\u0080\4\2\t\2\4"
            + "\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\3\2\7\2"
            + "\26\n\2\f\2\16\2\31\13\2\3\2\5\2\34\n\2\3\3\3\3\5\3 \n\3\3\4\3\4\3\4\3"
            + "\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5.\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3"
            + "\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5B\n\5\3\5\6\5E\n\5\r"
            + "\5\16\5F\3\5\3\5\3\5\3\5\3\5\5\5N\n\5\3\5\3\5\3\5\3\5\3\5\5\5U\n\5\3\5"
            + "\3\5\3\5\3\5\5\5[\n\5\3\5\3\5\5\5_\n\5\7\5a\n\5\f\5\16\5d\13\5\3\6\3\6"
            + "\5\6h\n\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\5\bt\n\b\3\t\3\t\3\t"
            + "\7\ty\n\t\f\t\16\t|\13\t\3\n\3\n\3\n\2\3\b\13\2\4\6\b\n\f\16\20\22\2\b"
            + "\3\2\21\24\3\2\17\20\3\2\25\32\3\2\33\35\4\2\20\20\36\37\3\2 %\2\u008d"
            + "\2\33\3\2\2\2\4\37\3\2\2\2\6!\3\2\2\2\b-\3\2\2\2\ne\3\2\2\2\fk\3\2\2\2"
            + "\16s\3\2\2\2\20u\3\2\2\2\22}\3\2\2\2\24\26\5\4\3\2\25\24\3\2\2\2\26\31"
            + "\3\2\2\2\27\25\3\2\2\2\27\30\3\2\2\2\30\34\3\2\2\2\31\27\3\2\2\2\32\34"
            + "\7\2\2\3\33\27\3\2\2\2\33\32\3\2\2\2\34\3\3\2\2\2\35 \5\6\4\2\36 \5\b"
            + "\5\2\37\35\3\2\2\2\37\36\3\2\2\2 \5\3\2\2\2!\"\7%\2\2\"#\7\3\2\2#$\5\b"
            + "\5\2$\7\3\2\2\2%&\b\5\1\2&.\5\f\7\2\'(\7\4\2\2()\5\b\5\2)*\7\5\2\2*.\3"
            + "\2\2\2+.\5\n\6\2,.\5\22\n\2-%\3\2\2\2-\'\3\2\2\2-+\3\2\2\2-,\3\2\2\2."
            + "b\3\2\2\2/\60\f\n\2\2\60\61\7\t\2\2\61a\5\b\5\13\62\63\f\b\2\2\63\64\t"
            + "\2\2\2\64a\5\b\5\t\65\66\f\7\2\2\66\67\t\3\2\2\67a\5\b\5\b89\f\6\2\29"
            + ":\t\4\2\2:a\5\b\5\7;<\f\5\2\2<=\t\5\2\2=a\5\b\5\6>D\f\f\2\2?A\7\4\2\2"
            + "@B\5\20\t\2A@\3\2\2\2AB\3\2\2\2BC\3\2\2\2CE\7\5\2\2D?\3\2\2\2EF\3\2\2"
            + "\2FD\3\2\2\2FG\3\2\2\2Ga\3\2\2\2HI\f\13\2\2IJ\7\6\2\2JM\5\b\5\2KL\7\7"
            + "\2\2LN\5\b\5\2MK\3\2\2\2MN\3\2\2\2NO\3\2\2\2OP\7\b\2\2Pa\3\2\2\2QT\f\t"
            + "\2\2RS\7\n\2\2SU\7$\2\2TR\3\2\2\2TU\3\2\2\2UV\3\2\2\2VW\7\13\2\2WZ\5\b"
            + "\5\2XY\7\f\2\2Y[\5\b\5\2ZX\3\2\2\2Z[\3\2\2\2[^\3\2\2\2\\]\7\r\2\2]_\5"
            + "\b\5\2^\\\3\2\2\2^_\3\2\2\2_a\3\2\2\2`/\3\2\2\2`\62\3\2\2\2`\65\3\2\2"
            + "\2`8\3\2\2\2`;\3\2\2\2`>\3\2\2\2`H\3\2\2\2`Q\3\2\2\2ad\3\2\2\2b`\3\2\2"
            + "\2bc\3\2\2\2c\t\3\2\2\2db\3\2\2\2eg\7\6\2\2fh\5\20\t\2gf\3\2\2\2gh\3\2"
            + "\2\2hi\3\2\2\2ij\7\b\2\2j\13\3\2\2\2kl\t\6\2\2lm\5\16\b\2m\r\3\2\2\2n"
            + "t\5\22\n\2op\7\4\2\2pq\5\b\5\2qr\7\5\2\2rt\3\2\2\2sn\3\2\2\2so\3\2\2\2"
            + "t\17\3\2\2\2uz\5\b\5\2vw\7\16\2\2wy\5\b\5\2xv\3\2\2\2y|\3\2\2\2zx\3\2"
            + "\2\2z{\3\2\2\2{\21\3\2\2\2|z\3\2\2\2}~\t\7\2\2~\23\3\2\2\2\21\27\33\37" + "-AFMTZ^`bgsz";
    public static final ATN _ATN = new ATNDeserializer().deserialize(_serializedATN.toCharArray());
    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}