/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.expression;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.simo.function.TestBase;
import org.simo.function.integration.model.Attribute;
import org.simo.function.integration.model.Value;

/**
 * @author Simo
 *
 */
public class ExpressionTest extends TestBase {

    @Test
    public void logicOperators() {
        assertEquals("true", run("1 < 2"));
        assertEquals("true", run("1 > 0"));
        assertEquals("true", run("1 == 1"));
        assertEquals("true", run("2 <= 2"));
        assertEquals("true", run("2 >= 2"));
        assertEquals("true", run("1 != 2"));
    }

    @Test
    public void quadraticEquation() {
        Object x0 = run("int((-3 + sqrt(3^2 - 4*-4))/2)");
        Object x1 = run("int((-3 - sqrt(3^2 - 4*-4))/2)");
        assertEquals("1", x0);
        assertEquals("-4", x1);
    }

    @Test
    public void chainedFunctionCalls() {
        Object result = run("int(if(1 == 1, sum, sqrt)(1))");
        assertEquals("1", result);
    }

    @Test
    public void whereExpression() {
        Object result = run("sum([1,2,3] as n where n > 1)");
        assertEquals("5.0", result);
    }

    @Test
    public void whereExpressionWithoutLocalVar() {
        Object result = run("sum([1,2,3] where _ > 1)");
        assertEquals("5.0", result);
    }

    @Test
    public void whereExpressionSelect() {
        Object result = run("sum([1,2,3,4,5,6,7,8,9,10] where _ % 2 == 0 and _ > 3 select _ * 2)");
        assertEquals("56.0", result);
    }

    @Test
    public void whereExpressionLimit() {
        Object result = run("sum([1,2,3] as n where n > 1 limit 1)");
        assertEquals("2.0", result);
    }

    @Test
    public void valueAttributeAccess() {
        Attribute jee = new Attribute("test", "1");
        Value value = new Value();
        value.addAttribute(jee);

        env.defineSymbol("globalvar", value);

        Object result = run("1 + double(globalvar.test)");
        assertEquals("2.0", result);
    }
}
