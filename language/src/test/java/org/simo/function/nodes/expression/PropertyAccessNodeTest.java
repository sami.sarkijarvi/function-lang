/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.expression;

import org.junit.Assert;
import org.junit.Test;
import org.simo.function.TestBase;

/**
 * @author Simo
 *
 */
public class PropertyAccessNodeTest extends TestBase {

    @Test
    public void propertyCalledReflectively() {
        env.defineSymbol("testValue", new TestValue());
        Object result = run("join(\";\", testValue.hello, testValue.world, testValue.sum)");
        Assert.assertEquals("hello;world;2", result);
    }

    class TestValue {

        public String hello() {
            return "hello";
        }

        public String world() {
            return "world";
        }

        public int sum() {
            return 1 + 1;
        }
    }
}
