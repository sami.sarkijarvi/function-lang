/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function;

import org.simo.function.integration.DefaultObjectCallSite;
import org.simo.function.nodes.Node;
import org.simo.function.parser.FunctionAST;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;

/**
 * @author Simo
 *
 */
public class TestUtils {

    private TestUtils() {
    }

    public static final Environment createEnvWithModelSupport() {
        DefaultObjectCallSite defaultObjectCallSite = new DefaultObjectCallSite();

        Environment env = Environment.create();
        env.setObjectCallSite(defaultObjectCallSite);

        return env;
    }

    public static final Object runCode(String code) {
        return runCode(code, Environment.create());
    }

    public static final Object runCode(String code, Environment env) {
        Node root = FunctionAST.parse(code);
        return root.eval(env);
    }

    public static final String toString(Object object) {
        StringBuilder sb = new StringBuilder();
        if (object instanceof FunctionValue) {
            sb.append(((FunctionValue) object).asString());
        }
        return sb.toString();
    }
}
