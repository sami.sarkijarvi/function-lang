/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.integration.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sniemela
 *
 */
public class Value {
    private List<Attribute> attributes;
    private boolean selected = false;
    private boolean allowed = false;
    private boolean conAllowed = false;
    private Parameter parameter;
    private int id;

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    public Parameter getParameter() {
        return parameter;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void addAttribute(Attribute attr) {
        if (attributes == null) {
            attributes = new ArrayList<>();
        }
        attributes.add(attr);
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public Attribute getAttributeByType(String attrType) {
        if (attributes == null) {
            return null;
        }
        for (Attribute attr : attributes) {
            if (attr.getType().equals(attrType)) {
                return attr;
            }
        }
        return null;
    }

    public boolean isAllowed() {
        return allowed;
    }

    public boolean isConAllowed() {
        return conAllowed;
    }

    public void setConAllowed(boolean conAllowed) {
        this.conAllowed = conAllowed;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
