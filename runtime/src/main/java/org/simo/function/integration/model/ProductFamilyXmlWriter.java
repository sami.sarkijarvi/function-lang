/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.integration.model;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Simo
 *
 */
public final class ProductFamilyXmlWriter {

    private static final Logger LOGGER = Logger.getLogger(ProductFamilyXmlWriter.class.getName());

    private StringBuilder sb;
    private OutputStream outputStream;
    private ProductFamily productFamily;

    public ProductFamilyXmlWriter(ProductFamily productFamily) {
        this.sb = new StringBuilder();
        this.productFamily = productFamily;
    }

    public void write(OutputStream outputStream) {
        this.outputStream = outputStream;
        start();
    }

    private void start() {
        writeProductFamily();
    }

    private void writeProductFamily() {
        write("<productfamily>");
        writeTabs(productFamily.getTabs());
        write("</productfamily>");
    }

    private void writeTabs(List<Tab> tabs) {
        write("<tabs>");
        tabs.forEach(this::writeTab);
        write("</tabs>");
    }

    private void writeTab(Tab tab) {
        write("<tab>");
        writeParameters(tab.getParameters());
        write("</tab>");
    }

    private void writeParameters(List<Parameter> parameters) {
        write("<parameters>");
        for (int i = 0, len = parameters.size(); i < len; i++) {
            writeParameter(parameters.get(i));
        }
        write("</parameters>");
    }

    private void writeParameter(Parameter parameter) {
        sb.append("<parameter id='");
        sb.append(parameter.getId());
        sb.append("'>");
        flush();
        writeValues(parameter.getValues());
        write("</parameter>");
    }

    private void writeValues(List<Value> values) {
        write("<values>");
        for (int i = 0, len = values.size(); i < len; i++) {
            writeValue(values.get(i));
        }
        write("</values>");
    }

    private void writeValue(Value value) {
        sb.append("<value id='");
        sb.append(value.getId());
        sb.append("'>");
        flush();
        writeAttributes(value.getAttributes());
        write("</value>");
    }

    private void writeAttributes(List<Attribute> attributes) {
        write("<attributes>");
        for (int i = 0, len = attributes.size(); i < len; i++) {
            writeAttribute(attributes.get(i));
        }
        write("</attributes>");
    }

    private void writeAttribute(Attribute attribute) {
        sb.append("<attribute type='");
        sb.append(attribute.getType());
        sb.append("' value='");
        sb.append(attribute.getValue());
        sb.append("' />");
        flush();
    }

    private void flush() {
        write(sb.toString());
        sb.setLength(0);
    }

    private void write(String value) {
        try {
            outputStream.write(value.getBytes("UTF-8"));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
