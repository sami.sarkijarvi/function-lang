/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.expression;

import java.util.ArrayList;
import java.util.List;

import org.simo.function.nodes.Node;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;

/**
 * @author Simo
 *
 */
public final class WhereNode extends Node {
    private final Node values;
    private final String localVar;
    private final Node conditions;
    private final Node select;
    private final Node limit;

    public WhereNode(Node values, String localVar, Node conditions, Node select, Node limit) {
        this.values = values;
        this.localVar = localVar;
        this.conditions = conditions;
        this.select = select;
        this.limit = limit;
    }

    @Override
    public FunctionValue eval(Environment env) {
        FunctionValue valueListFv = values.eval(env);
        List<? extends Object> list = valueListFv.asList();
        env.rfv(valueListFv);

        List<Object> items = new ArrayList<>(list.size());
        int itemLimit = -1;

        if (limit != null) {
            itemLimit = asInt(env, limit);
        }

        env.beginScope();

        for (Object obj : list) {
            if (itemLimit > -1 && items.size() >= itemLimit) {
                break;
            }

            defineSymbol(obj, env);

            if (asBoolean(env, conditions)) {
                items.add(select(obj, env));
            }
        }

        env.endScope();

        return env.fv().ofObjectList(items);
    }

    private Object select(Object obj, Environment env) {
        if (select == null) {
            return obj;
        }
        return asObject(env, select);
    }

    private void defineSymbol(Object obj, Environment env) {
        if (localVar != null && !localVar.isEmpty()) {
            env.defineSymbol(localVar, obj);
        } else {
            env.defineSymbol("_", obj);
        }
    }
}
