/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes;

import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;

/**
 * @author Simo
 *
 */
public abstract class Node {
    public abstract FunctionValue eval(Environment env);

    public static final boolean asBoolean(Environment env, Node node) {
        FunctionValue fv = node.eval(env);
        boolean bool = fv.asBoolean();
        env.rfv(fv);
        return bool;
    }

    public static final double asDouble(Environment env, Node node) {
        FunctionValue fv = node.eval(env);
        double value = fv.asDouble();
        env.rfv(fv);
        return value;
    }

    public static final Object asObject(Environment env, Node node) {
        FunctionValue fv = node.eval(env);
        Object value = fv.asObject();
        env.rfv(fv);
        return value;
    }

    public static final int asInt(Environment env, Node node) {
        FunctionValue fv = node.eval(env);
        int value = fv.asInt();
        env.rfv(fv);
        return value;
    }
}
