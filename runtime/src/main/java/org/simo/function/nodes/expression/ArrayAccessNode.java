/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.expression;

import java.util.List;

import org.simo.function.nodes.Node;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;

/**
 * @author Simo
 *
 */
public final class ArrayAccessNode extends Node {
    private final Node object;
    private final Node fromIndex;
    private final Node toIndex;

    public ArrayAccessNode(Node object, Node fromIndex, Node toIndex) {
        this.object = object;
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
    }

    @Override
    public FunctionValue eval(Environment env) {
        int index = asInt(env, fromIndex);
        Integer lastIndex = null;

        if (toIndex != null) {
            lastIndex = asInt(env, toIndex);
        }

        FunctionValue obj = object.eval(env);

        if (obj.isString()) {
            String value = obj.asString();
            env.rfv(obj);
            return env.fv().ofString(substring(value, index, lastIndex));
        }

        if (!obj.isList()) {
            env.rfv(obj);
            throw new IllegalArgumentException("Array access not support for non-list");
        }

        List<? extends Object> list = obj.asList();
        env.rfv(obj);

        if (lastIndex != null) {
            return env.fv().ofObjectList(list.subList(index, lastIndex));
        }

        return env.fv().ofObject(list.get(index));
    }

    private String substring(String obj, int index, Integer lastIndex) {
        if (lastIndex == null) {
            return Character.toString(obj.charAt(index));
        } else {
            return obj.substring(index, lastIndex);
        }
    }
}
