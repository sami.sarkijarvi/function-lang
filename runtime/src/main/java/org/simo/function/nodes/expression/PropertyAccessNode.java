/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.expression;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.simo.function.nodes.Node;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;
import org.simo.function.runtime.ObjectCallSite;
import org.simo.function.runtime.exceptions.UndefinedPropertyException;

/**
 * @author sniemela
 *
 */
public final class PropertyAccessNode extends Node {
    private final Node object;
    private final String property;
    private final String cacheKey;

    public PropertyAccessNode(Node object, String property) {
        this.object = object;
        this.property = property;
        this.cacheKey = object.toString() + "." + property;
    }

    @Override
    public FunctionValue eval(Environment env) {
        Object cacheKeySymbol = env.getSymbol(cacheKey);
        if (cacheKeySymbol != null) {
            return env.fv().ofObject(cacheKeySymbol);
        }

        if (property == null) {
            throw new NullPointerException();
        }

        Object returnValue = null;
        ObjectCallSite callSite = env.getObjectCallSite();
        Object valueObject = asObject(env, object);

        if (callSite != null && callSite.canCall(valueObject, property, env)) {
            returnValue = callSite.call(valueObject, property, env);
        } else {
            returnValue = invokeByReflection(valueObject, property);
        }

        env.defineSymbol(cacheKey, returnValue);

        return env.fv().ofObject(returnValue);
    }

    private Object invokeByReflection(Object obj, String prop) {
        try {
            Method method = obj.getClass().getMethod(prop);
            boolean resetAccessible = false;
            if (!method.canAccess(obj)) {
                method.setAccessible(true);
                resetAccessible = true;
            }
            Object result = method.invoke(obj);
            if (resetAccessible) {
                method.setAccessible(false);
            }
            return result;
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            throw new UndefinedPropertyException("Undefined property: " + prop);
        }
    }

}
