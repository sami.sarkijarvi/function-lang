/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.runtime;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.simo.function.runtime.exceptions.UndefinedSymbolException;

/**
 * @author Simo
 *
 */
public final class Environment {

    private final Stack<Map<String, Object>> varScopes = new Stack<>();
    private final Stack<Map<String, Object>> symbolScopes = new Stack<>();
    private final FunctionRegistry globalFunctionRegistry;
    private ObjectCallSite objectCallSite;
    private final FunctionValuePool functionValuePool;

    public static void main(String[] args) {
        new Environment(null, null).objectCallSite = null;
    }

    private Environment(FunctionRegistry globalFunctionRegistry, FunctionValuePool functionValuePool) {
        this.globalFunctionRegistry = globalFunctionRegistry;
        this.functionValuePool = functionValuePool;
    }

    public static final Environment create() {
        return create(new FunctionRegistry(), new FunctionValuePool());
    }

    public static final Environment create(FunctionRegistry globalFunctionRegistry,
            FunctionValuePool functionValuePool) {
        Environment env = new Environment(globalFunctionRegistry, functionValuePool);
        env.beginScope();
        return env;
    }

    public void setObjectCallSite(ObjectCallSite callSite) {
        this.objectCallSite = callSite;
    }

    public ObjectCallSite getObjectCallSite() {
        return objectCallSite;
    }

    public FunctionRegistry getFunctionRegistry() {
        return globalFunctionRegistry;
    }

    public void defineSymbol(String name, Object value) {
        define(name, value, symbolScopes);
    }

    public Object getSymbol(String name) {
        return get(name, symbolScopes);
    }

    public boolean hasSymbol(String name) {
        return get(name, symbolScopes) != null;
    }

    public void defineVar(String name, Object value) {
        define(name, value, varScopes);
    }

    public Object getVar(String name) {
        Object object = get(name, varScopes);
        if (object == null) {
            throw new UndefinedSymbolException("Undefined variable: " + name);
        }
        return object;
    }

    public boolean hasVar(String name) {
        return get(name, varScopes) != null;
    }

    private void define(String name, Object value, Stack<Map<String, Object>> scopes) {
        scopes.peek().put(name, value);
    }

    private Object get(String key, Stack<Map<String, Object>> scopes) {
        for (int i = scopes.size() - 1; i >= 0; i--) {
            Object obj = scopes.get(i).get(key);
            if (obj != null) {
                return obj;
            }
        }
        return null;
    }

    public void beginScope() {
        varScopes.push(new HashMap<>());
        symbolScopes.push(new HashMap<>());
    }

    public void endScope() {
        varScopes.pop();
        symbolScopes.pop();
    }

    public FunctionValue fv() {
        return functionValuePool.borrow();
    }

    public void rfv(FunctionValue value) {
        functionValuePool.release(value);
    }
}
