package org.simo.function.runtime.exceptions;

public class FunctionValueException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FunctionValueException(String message) {
        super(message);
    }
}
