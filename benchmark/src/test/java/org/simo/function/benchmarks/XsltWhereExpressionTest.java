/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.benchmarks;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.Assert;
import org.junit.Test;
import org.simo.function.benchmarks.util.ProductFamilyData;
import org.simo.function.integration.model.ProductFamily;
import org.simo.function.integration.model.ProductFamilyXmlWriter;

public class XsltWhereExpressionTest {

    @Test
    public void testWhereExpression() {
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(getClass().getResourceAsStream("where-expression-xslt.xml"));
        try {
            Transformer transformer = factory.newTransformer(xslt);
            Source input = new StreamSource(new ByteArrayInputStream(getProductFamilyData()));

            StringWriter resultWriter = new StringWriter();
            StreamResult result = new StreamResult(resultWriter);
            transformer.transform(input, result);

            String resultData = resultWriter.toString();
            Assert.assertEquals("2450", resultData);
        } catch (TransformerConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private byte[] getProductFamilyData() {
        ProductFamily family = ProductFamilyData.createExampleProductFamily();
        ProductFamilyXmlWriter xmlWriter = new ProductFamilyXmlWriter(family);
        byte[] data = new byte[0];
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            outputStream.write("<?xml version='1.0'?>\n".getBytes());
            xmlWriter.write(outputStream);
            data = outputStream.toByteArray();

            String debug = new String(data);
            System.out.println(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}
