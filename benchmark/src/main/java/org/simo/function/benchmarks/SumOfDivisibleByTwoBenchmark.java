/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.benchmarks;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.concurrent.TimeUnit;

import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.simo.function.integration.model.Attribute;
import org.simo.function.integration.model.Parameter;
import org.simo.function.integration.model.Value;
import org.simo.function.nodes.Node;
import org.simo.function.parser.FunctionAST;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(value = 1)
/*
 * jvmArgsAppend = { "-XX:+FlightRecorder",
        "-XX:StartFlightRecording=duration=60s,filename=E:/temp/profiling-data.jfr,name=FULL,settings=profile",
 */
public class SumOfDivisibleByTwoBenchmark extends BaseBenchmark {

    private Node functionAst;

    @Setup
    public void setup() {
        super.setup("where-expression-xslt.xml");

        env.defineSymbol("firstParameter", productFamily.getTabs().get(0).getParameters().get(0));

        functionAst = FunctionAST.parse(
                "sum(allvalues(firstParameter) as value where double(value.attr_0) % 2 == 0 select value.attr_0)");
    }

    @Benchmark
    public Object whereExpressionFunctionLang() {
        return functionAst.eval(env);
    }

    @Benchmark
    public String whereExpressionXslt() {
        StringWriter resultWriter = new StringWriter();
        StreamResult result = new StreamResult(resultWriter);
        try {
            transformer.transform(new StreamSource(new ByteArrayInputStream(productFamilyXmlData)), result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return resultWriter.toString();
    }

    @Benchmark
    public double whereExpressionJava() {
        return doBaseLine();
    }

    private double doBaseLine() {
        Parameter parameter = productFamily.getTabs().get(0).getParameters().get(0);
        double result = 0;
        for (Value value : parameter.getValues()) {
            Attribute attr0 = value.getAttributeByType("attr_0");
            double attr0Value = Double.parseDouble(attr0.getValue());

            if (attr0Value % 2 == 0) {
                result += attr0Value;
            }
        }
        return result;
    }
}
