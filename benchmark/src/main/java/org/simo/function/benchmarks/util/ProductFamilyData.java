/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.benchmarks.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.simo.function.integration.model.Attribute;
import org.simo.function.integration.model.Parameter;
import org.simo.function.integration.model.ProductFamily;
import org.simo.function.integration.model.ProductFamilyXmlWriter;
import org.simo.function.integration.model.Tab;
import org.simo.function.integration.model.Value;

/**
 * @author Simo
 *
 */
public class ProductFamilyData {

    public static final int PARAMETER_COUNT = 5;
    public static final int VALUE_COUNT = 100;
    public static final int ATTRIBUTE_COUNT = 30;

    public static void main(String[] args) {
        ProductFamily exampleFamily = createExampleProductFamily();
        try (FileOutputStream output = new FileOutputStream(new File("C:/temp/benchmarkdata.xml"))) {
            ProductFamilyXmlWriter writer = new ProductFamilyXmlWriter(exampleFamily);
            writer.write(output);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ProductFamily createExampleProductFamily() {
        ProductFamily family = new ProductFamily();
        Tab tab = new Tab();
        family.addTab(tab);

        for (int i = 0; i < PARAMETER_COUNT; i++) {
            Parameter parameter = new Parameter();
            parameter.setId(i);

            for (int j = 0; j < VALUE_COUNT; j++) {
                parameter.addValue(createValue(j));
            }

            tab.addParameter(parameter);
        }

        return family;
    }

    private static Value createValue(int id) {
        Value value = new Value();
        value.setId(id);

        for (int i = 0; i < ATTRIBUTE_COUNT; i++) {
            Attribute attr = new Attribute("attr_" + i, String.valueOf(i + id));
            value.addAttribute(attr);
        }

        return value;
    }
}
