package org.simo.function.benchmarks;

import java.io.IOException;
import java.io.StringWriter;
import java.util.concurrent.TimeUnit;

import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.simo.function.nodes.Node;
import org.simo.function.parser.FunctionAST;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(value = 1)
public class QuadraticEquationBenchmark extends BaseBenchmark {

    Node functionAst;

    int a = -3;
    int b = 3;
    int c = 2;
    int d = 4;
    int e = -4;
    int f = 2;

    @Setup
    public void setup() {
        super.setup("quadratic-xslt.xml");

        try {
            functionAst = FunctionAST.parse(QuadraticEquationBenchmark.class.getResourceAsStream("quadratic.fn"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Benchmark
    public String quadraticEquationFunctionLang() {
        return functionAst.eval(env).asString();
    }

    @Benchmark
    public String quadraticEquationXSLT() {
        StringWriter resultWriter = new StringWriter();
        StreamResult result = new StreamResult(resultWriter);
        try {
            transformer.transform(new StreamSource(), result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return resultWriter.toString();
    }

    @Benchmark
    public String quadraticEquationJava() {
        double x0 = (a + Math.sqrt(Math.pow(b, c) - d*e))/(f*d);
        double x1 = (a - Math.sqrt(Math.pow(b, c) - d*e))/(f*d);
        return x0 + " " + x1;
    }
}
